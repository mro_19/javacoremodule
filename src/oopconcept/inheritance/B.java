package oopconcept.inheritance;

public class B extends A {
    public static void main(String[] args) {
        B b = new B();
        b.methodFromA();
    }

    public void methoodFromB() {
        System.out.println("Method Details From B Class");
    }

    //method overriding
    @Override
    public void methodFromA() {
        System.out.println("Method Details From B Class");
    }
}

