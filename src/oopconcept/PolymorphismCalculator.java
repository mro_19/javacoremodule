package oopconcept;

public class PolymorphismCalculator {
    //polymorphism ---> having many form


    public static void main(String[] args) {
        PolymorphismCalculator calculator = new PolymorphismCalculator();
        calculator.addition(10, 15, 20);
        calculator.addition(10, 15);


    }
    //method overloading --> same method different params, same class
    //known as static polymorphism/compile time polymorphism

    public void addition(int x, int y, int z) {
        System.out.println(x + y + z);

    }

    public void addition(int x, int y) {
        System.out.println(x + y);

    }
    //method overriding --> same method name ,same param, different class
    // known as dynamic polymorphism /run time polymorphism
    //see example at Abstraction


}
