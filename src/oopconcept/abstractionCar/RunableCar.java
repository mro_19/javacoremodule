package oopconcept.abstractionCar;

public class RunableCar extends Car implements Vehicle, Vehicle2 {

    //interface --> Implements(I_I)
    //Class/Abstract -- Extent
    // extend first  ,then  implements
    //implement multiple interface
    //can't extend more than class/abstract class

    @Override
    public void move() {
        System.out.println("Car can move");

    }

    @Override
    public void start() {
        System.out.println("Car can Start");

    }

    @Override
    public void stop() {
        System.out.println("Car can Stop");

    }

    @Override
    public String name() {


        return "Honda";
    }

    @Override
    public void wheels() {
        System.out.println("Car hase 4 wheels");

    }


}
