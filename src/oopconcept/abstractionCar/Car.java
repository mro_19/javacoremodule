package oopconcept.abstractionCar;

public abstract class Car {
    public abstract void wheels();

    public void color() {
        System.out.println("Red");

    }
    // abstract class can have method with/without body
    // abstract keyword needs  to be specified in abstract methods
    //can have constructor


}
