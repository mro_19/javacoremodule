package oopconcept.abstractionCar;

public interface Vehicle {
    //interface is just an concept
    void move();

    void start();

    void stop();

    String name();

    //an interface can't have body
    //can't have a constructor
    // it can have return/void type method names -- >not body
}
