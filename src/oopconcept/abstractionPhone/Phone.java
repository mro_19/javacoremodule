package oopconcept.abstractionPhone;

public abstract class Phone {


    public abstract void charger();

    public abstract void memory();

    public abstract void bluetooth();

    public void color() {
        System.out.println("Blue");
    }

}
