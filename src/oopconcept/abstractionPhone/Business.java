package oopconcept.abstractionPhone;

public interface Business {

    void incoming();

    void outgoing();

    void network();

    String name();


}
