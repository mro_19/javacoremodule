package oopconcept.abstractionPhone;

public class UseablePhone extends Phone implements Business {

    @Override
    public void incoming() {
        System.out.println("Phone Can be able to receive calls ");

    }

    @Override
    public void outgoing() {
        System.out.println("Phone Can be able to call");

    }

    @Override
    public void network() {
        System.out.println("Phone need to have network for communication ");

    }

    @Override
    public String name() {
        return "Samsung";
    }

    @Override
    public void charger() {
        System.out.println("Phone can be able to charge");

    }

    @Override
    public void memory() {
        System.out.println("Phone need memory to store file apps etc");

    }

    @Override
    public void bluetooth() {
        System.out.println("Phone needs to have bluetooth for transfer data");

    }
}
