package oopconcept.abstractionPhone;

public class TestPhone {
    public static void main(String[] args) {
        UseablePhone useablePhone = new UseablePhone();
        useablePhone.incoming();
        useablePhone.outgoing();
        useablePhone.network();
        String s = useablePhone.name();
        System.out.println(s);
        useablePhone.bluetooth();
        useablePhone.charger();
        useablePhone.memory();
        useablePhone.color();
    }
}
