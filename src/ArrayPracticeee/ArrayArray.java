package ArrayPracticeee;

public class ArrayArray {
    public static void main(String[] args) {
        String[] name = {"Opu", "Adnan", "Sourov", "Rahman Bhai"};
        for (int i = 0; i < name.length; i++) {
            System.out.println(name[i]);
        }
        System.out.println("-------------------------");

        // Start Point; End Point; Increment/Decrement

        for (int i = name.length; i > 0; i--) {
            System.out.println(name[i - 1]);

        }
    }
}
