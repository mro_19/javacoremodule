package basicConcept;

public class WhileLOOPs {
    public static void main(String[] args) {
        int x = 0;
        while (x < 10) {
            System.out.println("While loop is running");
            if (x > 6) {
                break;
            }
            x++;


        }
    }
}
