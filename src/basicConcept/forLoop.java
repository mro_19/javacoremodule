package basicConcept;

public class forLoop {
    public static void main(String[] args) {
        System.out.println(10);
        //starting point; end point; increment/decrement
        for (int i = 0; i < 10; i++) {
            System.out.println("This is loop");
        }


        for (int k = 0; k <= 10; k++) {
            System.out.println("Java IS PRINTED");
            for (int l = 0; l <= 2; l++) {
                System.out.println("SELENIUM IS PRINTED");
            }
        }
    }

}
