package dataStructure;

import java.util.LinkedList;

public class LinkedListpractice {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("opu");
        linkedList.add("rahmna bhai");

        //linked list has first and last method
        //for removing they have same as adding
        linkedList.addFirst("ahmed");
        linkedList.addLast("rahman");

        //remove
        linkedList.removeFirst();
        linkedList.removeLast();
        System.out.println(linkedList);


        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println(linkedList.get(i));
        }
    }
}
