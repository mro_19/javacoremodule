package dataStructure;

import java.util.ArrayList;

public class InnerArrayList {
    public static void main(String[] args) {

        ArrayList<String> city = new ArrayList<>();
        city.add("rego park");
        city.add("ozone park");
        city.add("prospect park");


        ArrayList<String> zipcode = new ArrayList<>();
        zipcode.add("11111");
        zipcode.add("22222");
        zipcode.add("33333");
        zipcode.add("44444");

        ArrayList<ArrayList<String>> states = new ArrayList<>();

        states.add(city);
        states.add(zipcode);
        System.out.println(states);


        System.out.println(states.get(0).get(1));
        System.out.println("***********");


        for (int i = 0; i < states.size(); i++) {
            for (int j = 0; j < states.get(i).size(); j++) {
                System.out.println(states.get(i).get(j));
            }
        }


    }
}
