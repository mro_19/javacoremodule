package dataStructure;

import java.util.HashMap;
import java.util.Hashtable;

public class HashTablePrac {
    public static void main(String[] args) {
        Hashtable<String, Object> hashtable = new Hashtable<>();
        hashtable.put("name", " opu");
        hashtable.put("age", " 24");
        hashtable.put("Salary", " 130000");
        //hashtable is threadSafe
        //if multiple testcase are running in same time it will let other wait
        //hashtable does not take or contain any null key or  value
        System.out.println(hashtable);


        //can have null key with multiple null values
        HashMap<String, Object> map = new HashMap<>();
        map.put(null, "Something");
        map.put("otherThing", null);
        System.out.println(map);

    }
}
