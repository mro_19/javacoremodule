package dataStructure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class HashSetpractice {
    public static void main(String[] args) {
        //HashSet print unique value

        HashSet<String> set = new HashSet<>();
        set.add("opu");
        set.add("opu");
        set.add("rahman Bhai");


        ArrayList<String> list = new ArrayList<>(set);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        //Iterator is interface .. iterator is object ..    iterator is a method from HashSet

        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }
}
