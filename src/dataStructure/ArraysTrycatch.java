package dataStructure;

public class ArraysTrycatch {
    static String[] names = {"opu", "sourov", "adnan", "rahman"};

    public static void main(String[] args) throws Exception {
        String name = "opu";
        System.out.println(name);


        System.out.println(names[2]);
        //catch&not fail
        printMyArrays();
        //throw & fail
        printMyArrays2();

        //catch the exception,perform something something & fail
        printMyArrays3();


        //in exception we just want to perform we just call *ignore*

        //parent of exception is *Exception*
        // grand parent of exception is   *throwable class*


        for (int i = 0; i == names.length; i++) {
            System.out.println(names[i]);

        }
    }

    public static void printMyArrays() {

        try {
            System.out.println(names[5]);
        } catch (ArrayIndexOutOfBoundsException ee) {
            System.out.println("Failed");
        }


    }

    public static void printMyArrays2() throws Exception {
        System.out.println(names[5]);

    }

    public static void printMyArrays3() {
        try {
            System.out.println(names[5]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("exception in the loop, please correct the index");
            ex.printStackTrace();
        }
    }


}