package dataBase;

import java.sql.*;
import java.util.ArrayList;

public class SQLConnect_2 {
    public static void main(String[] args) throws SQLException {
        String username = "root";
        String password = "Appleipad5";
        String databaseName = "batch4";


        //jdbc:mysql  - driver name
        //localhost - server name
        //3306 - server port

        String url = "jdbc:mysql://localhost:3306/" + databaseName; //if server timezone error use ( ?serverTimezone=UTC );

        String query = "select * from student;";


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();

        while (table.next()) {
            id.add(table.getInt("id"));
            names.add(table.getString("name"));
            location.add(table.getString("location"));
        }
        System.out.println(id);
        System.out.println(names);
        System.out.println(location);


        //for column count
        ResultSetMetaData resultSetMetaData = table.getMetaData();
        int number = resultSetMetaData.getColumnCount();
        System.out.println(number);


        //closing the connection
        statement.close();
        connection.close();


    }
}
