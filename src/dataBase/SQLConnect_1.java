package dataBase;

import java.sql.*;

public class SQLConnect_1 {
    public static void main(String[] args) throws SQLException {
        String username = "root";
        String password = "Appleipad5";
        String databaseName = "batch4";


        //jdbc:mysql  - driver name
        //localhost - server name
        //3306 - server port

        String url = "jdbc:mysql://localhost:3306/" + databaseName; //if server timezone error use ( ?serverTimezone=UTC );

        String query = "select * from student;";


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        while (table.next()) {
            int id = table.getInt("id");
            String name = table.getString("name");
            String location = table.getString("location");
            System.out.println(id + " " + name + " " + location);
        }

        // closing the connection
        statement.close();
        connection.close();
    }
}
