package coremodule.constructor;

public class Statictest {
    static {
        System.out.println("Static block is running");
    }

    String name;

    public Statictest() {

    }

    public void printMyName() {
        System.out.println(name);
    }
}
