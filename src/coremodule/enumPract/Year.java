package coremodule.enumPract;

public class Year {
    Months months;


    public Year(Months months) {
        this.months = months;

    }

    public static void main(String[] args) {
        Year year = new Year(Months.January);
        year.weatherOfTheYear();
    }

    public void weatherOfTheYear() {
        switch (months) {
            case January:
                System.out.println("Cold");
                break;
            case February:
            case March:
            case April:
            case May:
                System.out.println("Its getting better");
                break;
            case June:
            case July:
                System.out.println("its summer ");
                break;
            default:
                System.out.println("not in calender");
        }
    }
}
