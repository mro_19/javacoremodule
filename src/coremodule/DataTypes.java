package coremodule;

public class DataTypes {
    //global variable
    //static variable can call directly to main method
    static String name = "opu";


    int number = 100;

    public static void main(String[] args) {
        System.out.println(name);
        DataTypes dataTypes = new DataTypes();
        String d = dataTypes.getMyName();
        System.out.println(d);
        dataTypes.printMyName();


    }

    //void method
    //it will just performs an action will not return
    public void printMyName() {
        System.out.println("my name is my name");
        System.out.println(number);

    }

    //return type method
    public String getMyName() {
        String name = "ahmed";
        return name;
    }
}
